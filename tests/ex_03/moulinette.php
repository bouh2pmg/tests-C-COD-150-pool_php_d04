<?php

include_once($argv[1] . "/Mars.php");

$snack = new chocolate\Mars();
$snack2 = new chocolate\Mars();

$planet = new planet\Mars();
$planet2 = new planet\Mars(3.5);

$snack3 = new chocolate\Mars();

$planet->setSize(8.4);

echo "Test setter and getter == " . $planet->getSize() . "\n";
echo "Test constructor and getter == " . $planet2->getSize() . "\n";
echo "3rd snack Id should be [2] == " . $snack3->getId() . "\n";
