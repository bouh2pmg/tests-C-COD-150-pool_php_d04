<?php

include_once($argv[1] . "/Team.php");
if (!class_exists("Chocolate\Mars"))
    include_once($argv[1] . "/Mars.php");

$mutta = new Astronaut("Mutta");
$hibito = new Astronaut("Hibito");
$serika = new Astronaut("Serika");
$kenji = new Astronaut("Kenji");

$spaceBro = new Team("SpaceBrothers");

$spaceBro->add($mutta);
$spaceBro->add($hibito);
$spaceBro->add($serika);
$spaceBro->add($mutta);
$spaceBro->add($kenji);
$spaceBro->add(3);

echo "Count Members should be 4 ==> " . $spaceBro->countMembers() . "\n";

$titi = new planet\Mars();
$mutta->doActions($titi);
$kenji->doActions(23);

for ($i = 0; $i < 10; ++$i)
    {
        new Chocolate\Mars();
        if ($i % 2 == 0)
            $hibito->doActions(new Chocolate\Mars());
        else if ($i % 3 == 0)
            $serika->doActions(new Chocolate\Mars());
    }

$spaceBro->showMembers();
$spaceBro->remove($hibito);
echo "Count Members should be 3 ==> " . $spaceBro->countMembers() . "\n";

$spaceBro->remove($hibito);
$spaceBro->remove($kenji);

$spaceBro->showMembers();
$spaceBro->remove($serika);
$spaceBro->remove($serika);
$spaceBro->showMembers();

$spaceBro->remove($mutta);
$spaceBro->showMembers();