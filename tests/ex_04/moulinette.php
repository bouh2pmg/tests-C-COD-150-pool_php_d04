<?php

include_once($argv[1] ."/Mars.php");
include_once($argv[1] ."/Astronaut.php");

$mutta = new Astronaut("Mutta");
$hibito = new Astronaut("Hibito");
$serika = new Astronaut("Serika");

for ($i = 0; $i < 10; ++$i)
    {
        new Chocolate\Mars();
        if ($i % 2 == 0)
            $hibito->doActions(new Chocolate\Mars());
        else if ($i % 3 == 0)
            $serika->doActions(new Chocolate\Mars());
    }

$mutta->doActions();
$dest = new Planet\Mars(3.5);
$mutta->doActions($dest);
if ($mutta->getDestination() != $dest)
    echo "Bad destination, your Astronaut is lost in space.\n";

$mutta->doActions(new Planet\Mars(4.2));

echo "Check Planet Size == " . $mutta->getDestination()->getSize() . "\n";